#DEFAULT, please override in comandline
NAME := new

THEME_DIR := ../../../themes/custom
OLD_THEME_NAME := m3_custom_theme
NEW_THEME_NAME := m3_$(NAME)_theme
INPUT_THEME := themes/custom/$(OLD_THEME_NAME)
OUTPUT_THEME := $(THEME_DIR)/$(NEW_THEME_NAME)
THEME_INFO_NAME_ORIGINAL := Matrushka standard CMS (M-III) custom theme
THEME_INFO_NAME_REPLACEMENT := Theme for $(NAME) project
THEME_INFO_DESC_ORIGINAL := Drupal boilerplate theme for project bootstrapping that extends the M-III recipe base theme
THEME_INFO_DESC_ORIGINAL := Drupal theme for $(NAME) project, extended from M-III recipe custom theme
THEME_INFO_LOGO_ORIGINAL := Logotipo
THEME_INFO_LOGO_REPLACEMENT := $(NAME)
THEME_INFO_ASSETS_ORIGINAL := \/profiles\/contrib\/matrushka-standard-cms\/themes
THEME_INFO_ASSETS_REPLACEMENT := \/themes

MODULE_DIR := ../../../modules/custom
OLD_MODULE_KEY := m3_sample
NEW_MODULE_KEY := m3_$(NAME)
OLD_MODULE_LABEL := M3 Sample
NEW_MODULE_LABEL := $(NAME)
OLD_LAYOUT_OVERRIDES_MODULE_NAME := m3_sample_layout_overrides
NEW_LAYOUT_OVERRIDES_MODULE_NAME := m3_$(NAME)_layout_overrides
INPUT_LAYOUT_OVERRIDES_MODULE := modules/custom/m3_page_builder/samples/$(OLD_LAYOUT_OVERRIDES_MODULE_NAME)
OUTPUT_LAYOUT_OVERRIDES_MODULE := $(MODULE_DIR)/$(NEW_LAYOUT_OVERRIDES_MODULE_NAME)
OLD_CUSTOM_BLOCKS_MODULE_NAME := m3_sample_custom_blocks
NEW_CUSTOM_BLOCKS_MODULE_NAME := m3_$(NAME)_custom_blocks
INPUT_CUSTOM_BLOCKS_MODULE := modules/custom/m3_freeform/samples/$(OLD_CUSTOM_BLOCKS_MODULE_NAME)
OUTPUT_CUSTOM_BLOCKS_MODULE := $(MODULE_DIR)/$(NEW_CUSTOM_BLOCKS_MODULE_NAME)

compile-custom-theme-code:
	make -C $(OUTPUT_THEME) build

copy-custom-theme-code:
	mkdir -p $(THEME_DIR)
	cp -r $(INPUT_THEME) $(OUTPUT_THEME)
	find $(OUTPUT_THEME)/ -name '*$(OLD_THEME_NAME)*.*' -type f -exec bash -c 'mv "$$1" "$${1/$(OLD_THEME_NAME)/$(NEW_THEME_NAME)}"' -- {} \;
	find $(OUTPUT_THEME)/ -type f -exec sed -i 's/\(.*\)$(OLD_THEME_NAME)\(.*\)/\1$(NEW_THEME_NAME)\2/' {} \;
	sed -i 's/$(THEME_INFO_NAME_ORIGINAL)/$(THEME_INFO_NAME_REPLACEMENT)/' $(OUTPUT_THEME)/$(NEW_THEME_NAME).info.yml
	sed -i 's/$(THEME_INFO_DESC_ORIGINAL)/$(THEME_INFO_DESC_ORIGINAL)/' $(OUTPUT_THEME)/$(NEW_THEME_NAME).info.yml
	sed -i 's/$(THEME_INFO_LOGO_ORIGINAL)/$(THEME_INFO_LOGO_REPLACEMENT)/' $(OUTPUT_THEME)/logo.svg
	sed -i 's/$(THEME_INFO_ASSETS_ORIGINAL)/$(THEME_INFO_ASSETS_REPLACEMENT)/' $(OUTPUT_THEME)/sass/main.scss
	sed -i '/# Ignore vendor/d' $(OUTPUT_THEME)/.gitignore
	sed -i '/assets\/vendor/d' $(OUTPUT_THEME)/.gitignore
	sed -i '/# Ignore css build/d' $(OUTPUT_THEME)/.gitignore
	sed -i '/assets\/css/d' $(OUTPUT_THEME)/.gitignore

clone-custom-theme: copy-custom-theme-code compile-custom-theme-code

create-custom-module-dir:
	mkdir -p $(MODULE_DIR)

copy-layout-overrides-module:
	cp -r $(INPUT_LAYOUT_OVERRIDES_MODULE) $(OUTPUT_LAYOUT_OVERRIDES_MODULE)
	find $(OUTPUT_LAYOUT_OVERRIDES_MODULE)/ -name '*$(OLD_LAYOUT_OVERRIDES_MODULE_NAME)*.*' -type f -exec bash -c 'mv "$$1" "$${1/$(OLD_LAYOUT_OVERRIDES_MODULE_NAME)/$(NEW_LAYOUT_OVERRIDES_MODULE_NAME)}"' -- {} \;
	find $(OUTPUT_LAYOUT_OVERRIDES_MODULE)/ -type f -exec sed -i 's/\(.*\)$(OLD_LAYOUT_OVERRIDES_MODULE_NAME)\(.*\)/\1$(NEW_LAYOUT_OVERRIDES_MODULE_NAME)\2/' {} \;
	find $(OUTPUT_LAYOUT_OVERRIDES_MODULE)/ -type f -exec sed -i 's/\(.*\)$(OLD_MODULE_KEY)\(.*\)/\1$(NEW_MODULE_KEY)\2/' {} \;
	find $(OUTPUT_LAYOUT_OVERRIDES_MODULE)/ -type f -exec sed -i 's/\(.*\)$(OLD_MODULE_LABEL)\(.*\)/\1$(NEW_MODULE_LABEL)\2/' {} \;

copy-custom-blocks-module:
	cp -r $(INPUT_CUSTOM_BLOCKS_MODULE) $(OUTPUT_CUSTOM_BLOCKS_MODULE)
	find $(OUTPUT_CUSTOM_BLOCKS_MODULE)/ -name '*$(OLD_CUSTOM_BLOCKS_MODULE_NAME)*.*' -type f -exec bash -c 'mv "$$1" "$${1/$(OLD_CUSTOM_BLOCKS_MODULE_NAME)/$(NEW_CUSTOM_BLOCKS_MODULE_NAME)}"' -- {} \;
	find $(OUTPUT_CUSTOM_BLOCKS_MODULE)/ -type f -exec sed -i 's/\(.*\)$(OLD_CUSTOM_BLOCKS_MODULE_NAME)\(.*\)/\1$(NEW_CUSTOM_BLOCKS_MODULE_NAME)\2/' {} \;
	find $(OUTPUT_CUSTOM_BLOCKS_MODULE)/ -type f -exec sed -i 's/\(.*\)$(OLD_MODULE_KEY)\(.*\)/\1$(NEW_MODULE_KEY)\2/' {} \;
	find $(OUTPUT_CUSTOM_BLOCKS_MODULE)/ -type f -exec sed -i 's/\(.*\)$(OLD_MODULE_LABEL)\(.*\)/\1$(NEW_MODULE_LABEL)\2/' {} \;

clone-custom-modules: create-custom-module-dir copy-layout-overrides-module copy-custom-blocks-module
	