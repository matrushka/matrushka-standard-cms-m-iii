<?php

/**
 * @file
 * Hooks provided by the m3_core module.
 */

/**
 * Discover available paragraphs for contents
 *
 * Modules may implement this hook if they want to provide paragraph models for contents
 * subscribed to the "recipients" list, wich should be nodes with the layout paragraphs model enabled and the content field.
 *
 * @param array[] $content_paragraphs
 *   The array of available paragraph types to build contents, refferenced by ID.
 *
 */
function hook_discover_content_paragraphs(&$content_paragraphs){
  $currentModule = \Drupal::moduleHandler()->getModule('MODULE_NAME');

  $content_paragraphs['PARAGRAPH_ID'] = [
    'provider' => $currentModule->getName(),
    'black_list' => [], // array of recipient.fields that would be ignored for this content paragraph configuration
    'white_list' => NULL, // will ignore black_list if not null. Array of recipient.fields that would be exclusive for this paragraph
    /** 
     * Provide elements for white_list and black_list as follows: RECIPIENT_ID.FIELD_ID
     * If only RECIPIENT_ID is provided, any fileds would be black/white listed
    */
  ];
}

/**
 * Discover available recipients for contents
 *
 * Modules may implement this hook if they want to register a recipient for content_paragraphs
 *
 * @param array[] $content_recipients
 *   The array of recipient nodes that would recieve configured paragraphs, refferenced by ID.
 *
 */
function hook_discover_content_recipients(&$content_recipients){
  $currentModule = \Drupal::moduleHandler()->getModule('MODULE_NAME');

  $content_recipients[$currentModule->getName()] = [
      // an array of recipient.field names that would be included in the configuration process
  ];
}
