<?php

namespace Drupal\m3_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Copyright' Block.
 *
 * @Block(
 *   id = "footer_copyright_block",
 *   admin_label = @Translation("Footer Copyright"),
 *   category = @Translation("M-III custom blocks"),
 * )
 */
class FooterCopyrightBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'footercopyright';
    return array(
      '#theme' => 'freeform',
      '#code' => $code
    );
  }

}
