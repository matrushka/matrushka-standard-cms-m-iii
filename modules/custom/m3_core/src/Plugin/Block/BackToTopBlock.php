<?php

namespace Drupal\m3_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Back to Top' Block.
 *
 * @Block(
 *   id = "back_to_top_block",
 *   admin_label = @Translation("Back to Top"),
 *   category = @Translation("M-III custom blocks"),
 * )
 */
class BackToTopBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $code = 'backtotop';
    return array(
      '#theme' => 'freeform',
      '#code' => $code
    );
  }

}
