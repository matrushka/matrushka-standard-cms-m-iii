<?php

namespace Drupal\m3_core\Commands;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\m3_core\Commands
 */
class CoreCommands extends DrushCommands {

  /**
   * Drush command that displays the given text.
   *
   * @param string $text
   *   Argument with message to be displayed.
   * @command m3_core:message
   * @aliases m3-message m3-msg
   * @option uppercase
   *   Uppercase the message.
   * @option reverse
   *   Reverse the message.
   * @usage m3_core:message --uppercase --reverse drupal8
   */
  public function message($text = 'Hello world!', $options = ['uppercase' => FALSE, 'reverse' => FALSE]) {
    if ($options['uppercase']) {
      $text = strtoupper($text);
    }
    if ($options['reverse']) {
      $text = strrev($text);
    }
    $this->output()->writeln($text);
  }

  /**
   * Drush command that triggers paragraphs discovery the proceed to configure all discovered entities
   *
   * @command m3_core:paragraphs-discovery
   * @aliases m3-paragraphs-discovery m3-p-discover
   * @option confirmed
   *   Confirm in advance that discovered paragraphs should be configured.
   * @usage m3_core:paragraphs-discovery --confirmed
   */
  public function discovery($options = ['confirmed' => FALSE]) {
    $discoveries = m3_content_discovery();

    if(!$options['confirmed']) {
      $this->output()->writeln(":: Following content paragraphs will be enabled ::");
      foreach($discoveries['recipe'] as $recipient => $paragraphs) {
        $this->output()->writeln("  ".$recipient." -> [");
        foreach($paragraphs as $paragraph) {
          $this->output()->writeln("    ".$paragraph.",");
        }
        $this->output()->writeln("  ]\n");
      }
      $options['confirmed'] = $this->io()->confirm("Is this what you want?", true);
    }
    
    if($options['confirmed']){
      foreach($discoveries['recipe'] as $recipient => $paragraphs) {
        $this->output()->writeln("Aplying changes to ".$recipient);
        m3_content_connect($recipient, $paragraphs);
      }
    }
  }
}