<?php

namespace Drupal\m3_search_tools\Form;

use \Drupal;
use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Url;
use \Drupal\Core\Form\FormStateInterface;

class Page404Search extends FormBase {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#prefix'] = '<div id="Page-404-search">';
    $form['#suffix'] = '</div>';

    $form['fields'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['fields-container'],
      ]
    ];

    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['actions-container'],
      ]
    ];

    $fields = &$form['fields'];
    $actions = &$form['actions'];

    $fields['keywords'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search by keywords'),
      '#required' => TRUE
    ];

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['form-actions'],
      ]
    ];

    return $form;
  }

  public function getFormId() {
    return 'balance_custom_blocks_page_404_seearch_form';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $keywords = $form_state->getValue('keywords');
    $url = Url::fromRoute('view.search.page', ['search_api_fulltext' => $keywords]);
    $form_state->setRedirectUrl($url);
    return $form;
  }
}

