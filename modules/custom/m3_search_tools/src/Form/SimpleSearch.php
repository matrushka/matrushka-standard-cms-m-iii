<?php

namespace Drupal\m3_search_tools\Form;

use \Drupal\Core\Form\FormBase;
use \Drupal\Core\Form\FormStateInterface;

class SimpleSearch extends FormBase {
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['keywords'] = [
      '#type' => 'search',
      '#title' => $this->t('Keywords')
      // '#description' => $this->t('Search field description, if needed.')
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search')
    ];

    return $form;
  }

  public function getFormId() {
    return 'm3_search_tools_simple_search_form';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $options = [
      'query' => [
        'search_api_fulltext' => $values['keywords']
      ]
    ];

    $search_page = \Drupal\Core\Url::fromRoute('view.search.page', [], $options);
    $redirect = new \Symfony\Component\HttpFoundation\RedirectResponse($search_page->toString(), 307);
    $redirect->send();
  }
}
