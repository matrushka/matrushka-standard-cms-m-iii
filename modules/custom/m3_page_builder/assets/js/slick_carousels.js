(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.slickCarousels = {
    attach: function (context, settings) {

      var $slickCarouselsMultiple6 = $('.carousel-wrapper--6 .slick-carousel', context);
      if($slickCarouselsMultiple6.length) {
        $slickCarouselsMultiple6.slick({
          slidesToShow: 6,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          centerMode: true,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 786,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            },
          ]
        });
      }
      
      var $slickCarouselsMultiple5 = $('.carousel-wrapper--5 .slick-carousel', context);
      if($slickCarouselsMultiple5.length) {
        $slickCarouselsMultiple5.slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          centerMode: true,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 786,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            },
          ]
        });
      }

      var $slickCarouselsMultiple4 = $('.carousel-wrapper--4 .slick-carousel', context);
      if($slickCarouselsMultiple4.length) {
        $slickCarouselsMultiple4.slick({
          slidesToShow: 4,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          centerMode: true,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 786,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            },
          ]
        });
      }

      var $slickCarouselsMultiple3 = $('.carousel-wrapper--3 .slick-carousel', context);
      console.log($slickCarouselsMultiple3);
      if($slickCarouselsMultiple3.length) {
        $slickCarouselsMultiple3.slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 4000,
          centerMode: true,
          responsive: [
            {
              breakpoint: 786,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 576,
              settings: {
                slidesToShow: 1
              }
            },
          ]
        });
      }

      var $slickCarouselsSingle = $('.slick-carousel--single', context);
      if($slickCarouselsSingle.length) {
        $slickCarouselsSingle.slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          autoplay: true,
          autoplaySpeed: 4000,
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);