(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.navbarControl = {
      attach: function (context, settings) {
        var $headerWrapper = $(".header__wrapper", context);
        
        if($headerWrapper.length > 0) {
          $(window).on('load', function() {
            // Initial scroll offset and header wrapper offset
            let initialScrollOffset = $(document).scrollTop();
            var headerWrapperOffset = $headerWrapper.height() + $headerWrapper.offset().top - initialScrollOffset;
            
            // Verifying initial hash
            let initialHash = location.hash;
            if(initialHash.length > 0) {
              scrollToAnchor(initialHash.substr(1));
            }

            // Mobile navbar toggler process
            var isMobileMenuExpanded = false;
            var $navBarToggler = $headerWrapper.find('.navbar-toggler');
            var $navBarWrapper = $headerWrapper.find('.navbar__wrapper');
            $navBarToggler.click(function(e){
              e.preventDefault();
              $navBarWrapper.css('padding-top', ($headerWrapper.outerHeight() + $headerWrapper.position().top) + 'px');
              $headerWrapper.toggleClass('is-expanded');
              $navBarWrapper.toggleClass('is-expanded');
              isMobileMenuExpanded = $navBarWrapper.hasClass('is-expanded');
            });

            // Content table process
            var $contentTable = $(".content-table__table", context);
            var $pageTitle = $(".content__title", context).first();
            if($contentTable.length > 0 && $pageTitle.length > 0) {
              // Content table offset and trigger height
              var contentTableOffset = $contentTable.height() + $contentTable.offset().top;
              var triggerHeight = contentTableOffset - headerWrapperOffset;

              // Get list of anchor triggers
              var $contentList = $contentTable.find('a');
              var anchorList = $contentList.map(function(index, el){
                var anchorName = $(el).attr('href').substr(1);
                var $anchor = $('a[name='+ anchorName +']');
                var anchorOffset = $anchor.height() + $anchor.offset().top;
                var anchorTrigger = anchorOffset - headerWrapperOffset - 80;
                return {
                  name: anchorName,
                  trigger: anchorTrigger
                };
              }).get();
              
              // Cloning contents
              var $contentTableContents = $contentTable.clone();
              var $pageTitleContents = $pageTitle.clone();
              $headerWrapper.find('.navigation__content-table').append($contentTableContents);
              $headerWrapper.find('.navigation__content-title').append($pageTitleContents);

              // Initializing control variables
              var lastIndexName = null;
              var latestScrollPosition = 0;
              var contentTableClicked = false;
            }

            // Behavior functions
            function updateContentTableIndex(selectedAnchor) {
              var $currentAnchor = $headerWrapper.find('.navigation__content-table a[href="#'+ selectedAnchor +'"]');
              var $contentTableTable = $headerWrapper.find('.content-table__table');
              var horizontalOffset = $contentTableTable.find('a').first().offset().left;
              var currentLeftScroll = $currentAnchor.offset().left - horizontalOffset;
              $contentTableTable.find('a').removeClass('is-active')
              $currentAnchor.addClass('is-active');
              $contentTableTable.scrollLeft(currentLeftScroll);
            }

            function analizeScroll(currentScroll, goingUp) {
              if(currentScroll >= triggerHeight && !goingUp && !isMobileMenuExpanded) {
                $headerWrapper.addClass('is-shrinked');
              } else if(!contentTableClicked) {
                $headerWrapper.removeClass('is-shrinked');
              }
              var selectedAnchor = null;
              $.map(anchorList, function(el, index){
                if(currentScroll >= el.trigger) {
                  selectedAnchor = el.name;
                } else {
                  return false;
                }
              });
              if(lastIndexName != selectedAnchor) {
                updateContentTableIndex(selectedAnchor);
              }
            }

            function scrollToAnchor(anchorName) {
              contentTableClicked = true;
              setTimeout(function() {
                let anchor = anchorList.find(function(el){
                  return el.name == anchorName;
                });
                let scrollDistance = anchor.trigger + 20;
                $(document).scrollTop(scrollDistance);
              }, 10);
              setTimeout(function() {
                contentTableClicked = false;
              }, 30);
            }

            // Scroll event trigger
            $(document).scroll(function(e){
              let currentScroll = $(document).scrollTop();
              let goingUp = currentScroll <= latestScrollPosition;
              latestScrollPosition = currentScroll;
              analizeScroll(currentScroll, goingUp);
            });

            // Hash event trigger
            $(window).on('hashchange',function(e){
              let anchorName = e.currentTarget.location.hash.substr(1);
              scrollToAnchor(anchorName);
            });
          });
        }
      }
    }
  })(jQuery, Drupal, drupalSettings);