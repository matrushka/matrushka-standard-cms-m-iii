(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.homepageHeaderTransformer = {
    attach: function (context, settings) {

      var $headerWrapper = $(".header__wrapper", context);
      
      if($headerWrapper.length > 0) {
        var validateScroll = function(ev) {
          var currentScrollPosition = $(document).scrollTop();
          if(currentScrollPosition > 150) {
            $headerWrapper.addClass('is-reverted');
          }
          else {
            $headerWrapper.removeClass('is-reverted');
          }
        }
        
        $(document).on('scroll', validateScroll);

        validateScroll();
      }

    }
  }
})(jQuery, Drupal, drupalSettings);