/** 
 * Objetives:
 * 1. the function will detect an HTML attribute data-toggle-list, with a list of classes to toggle the element at
 * 2. if no attribute data-toggle-event is present, click() will be default
 * 3. toggle-list will have a comma separated list,
 *    a) If toggle list has only one class, it will toggle on and off on every event ocurrence.
 *    b) If toggle list has multiple classes, it will compare the actual classes of the object and if any of the list is present, it will be used as an index point to define wich class will follow in the sequence to be added once the event is triggered.
 *    c) If none of the classes from the toggle list is applied to the element, the firs class will be applied once the event is triggered.
 *    d) If more than one class is applied on the object, them will be replaced independiently following their own sequence as defined by the toggle list on every event ocurrence. 
*/

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.universalToggler = {
    attach: function (context, settings) {
      $togglers = $("[data-toggle-list]", context);
      $togglers.each(function (index){
        var $el = $(this);
        var toggleList = $el.data("toggle-list").replace(/ /g,"").split(",");
        var toggleList = $.grep(toggleList, function(value, index){
          return value != "";
        });

        // Get trigger event
        var toggleOn = $el.data("toggle-on");
        toggleOn = toggleOn ? toggleOn : "click";

        setCurrentAndReplaceLists($el, toggleList);

        // Get override others setting; if true, configured event will include the preventDefault() directive
        var overrideBehavior = $el.data("toggle-override-action");
        overrideBehavior = overrideBehavior ? overrideBehavior : false;

        // Get unique setting; if true, parameters for toggling will be mirrored from matching siblings (baed on root classes)
        var uniqueEnabled = $el.data("toggle-unique-among-siblings");
        uniqueEnabled = uniqueEnabled ? uniqueEnabled : false;

        $el.on(toggleOn, function(event) {
          if(overrideBehavior) {
            event.preventDefault();
          }
          if(uniqueEnabled) {
            clearSiblings($el, toggleList);
          }
          $el.removeClass($el.data("current-list"));
          $el.addClass($el.data("replace-list"));
          setCurrentAndReplaceLists($el, toggleList);
        });

        // Clear siblings to remove the current set of toggling clsses if required
        function clearSiblings($el, toggleList) {
          let refferenceList = $el.attr('class').split(" ").filter(key => !toggleList.includes(key));
          let selector = refferenceList.map(function(item){ return "." + item }).join(" ");
          let selection = $(selector, context);

          $el.data("toggler-axis", true);
          selection = selection.filter(function() {return !$(this).data("toggler-axis")});
          $el.removeData("toggler-axis");

          selection.each(function(){
            $(this).removeClass(toggleList);
            setCurrentAndReplaceLists($(this), toggleList);
          });
        }

        // Determine current and replace list based on object conditions
        function setCurrentAndReplaceLists($el, toggleList) {
          var currentList = [];
          var replaceList = [];

          // Check if toggle all is enabled or enable it if toggle list contains a single item
          let toggleAll = $el.data("toggle-all");
          toggleAll = toggleAll || toggleList.length == 1 ? true : false;

          $.each(toggleList, function(index, value){
            let exists = $el.hasClass(value);
            if(exists) {
              currentList.push(value);
              var newIndex = index + 1;
              if (newIndex >= toggleList.length) {
                newIndex = 0;
              }
              replaceList.push(toggleList[newIndex]);
            }
          });

          // If no class from toggle list was present, use first element
          if(replaceList.length == 0) {
            replaceList.push(toggleList[0]);
          }

          // console.log(toggleAll);
          if(toggleAll) {
            // Load complete toggle list in replace list
            replaceList = toggleList;

            // Filter present classes from the current list
            replaceList = $.grep(replaceList, function(value, index){
              return currentList.indexOf(value) > -1;
            }, true);
            // console.log(currentList);
            // console.log(replaceList);
          }
          
          // Store current and replace lists on object
          $el.data("current-list", currentList.join(" "));
          $el.data("replace-list", replaceList.join(" "));
        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);