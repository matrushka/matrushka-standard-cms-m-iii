(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.backToTopButtonControl = {
      attach: function (context, settings) {
        var $backToTopButton = $(".back-to-top-button__button", context);
        var minHeightToEnable = 920;
        var showAtScrollPercentage = 0.75;
        var maxScrollToForce = 3000;
  
        if($backToTopButton.length > 0) {
          var $backToTopContainer = $backToTopButton.parents(".back-to-top-button");

          function resetScroll(e) {
            e.preventDefault();
            window.scroll(0,0);
          }

          function showScrollToTop() {
            $backToTopContainer.addClass("is-available");
          }

          function hideScrollToTop() {
            $backToTopContainer.removeClass("is-available");
          }

          function compareScroll(currentScroll, windowHeight) {
            var factor = currentScroll / windowHeight;
            if((windowHeight > minHeightToEnable && factor > showAtScrollPercentage) || currentScroll > maxScrollToForce) {
              showScrollToTop();
            } else {
              hideScrollToTop();
            }
          }

          $backToTopButton.on('click', resetScroll);

          window.addEventListener('scroll', function(e) {
            compareScroll(window.scrollY, window.innerHeight);
          });
        }
      }
    }
  })(jQuery, Drupal, drupalSettings);