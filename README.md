# Matrushka Standard CMS (M-III) - Drupal 9 installation profile

Requisites:  
- A working instance of Drupal 9 (drupal-composer)  
- A web server or cointainer running, with php 7.x and composer installed  

### Configure your composer.json
It is required to set a few things in your Drupal 9 current composer.json in order to download and install this Profile as a dependency.

First add it as a repository. Go to the `repositories` section of the `composer.json` file in your root directory and add:

~~~~~
...
{
    "type": "git",
    "url": "git@bitbucket.org:matrushka/matrushka-standard-cms-m-iii.git"
}
...
~~~~~

Aditionally, it seems necesary to add another repository to the `repositories` section, in order to install some dependencies of Matrushka M-III. Add the following:

~~~~~
...
{
    "type": "package",
    "package": {
      "name": "ckeditor/div",
      "version": "4.10.1",
      "type": "drupal-library",
      "extra": {
        "installer-name": "ckeditor/plugins/div"
      },
      "dist": {
        "url": "https://download.ckeditor.com/div/releases/div_4.10.1.zip",
        "type": "zip"
      }
    }
}
...
~~~~~

And finally, execute the following command in your terminal, while being at the root folder fo your project:

~~~~~
composer require matrushka/matrushka-standard-cms:dev-release
~~~~~

Where `dev-release` is the tag used to identify the branch `release`. In case you need to install another branch , replace that word, keeping the prefix `dev-`. If you will require a versioned tag of the project (when released), just include the tag instead.


### Installing composer (if needed)
After the previous configuration, some modules will be added (Matrushka M-III dependencies). To ensure everything we need is installed, run `composer install`.

### Installing new Drupal modules
It is recommended to manage new required Drupal modules with composer. In order to register a new drupal module in the `composer.json` file of the root of your project, use the following command:

~~~~~
composer require "drupal/module_name"
~~~~~

That will download module files under `modules/contrib` and update `composer.json` with the new module name as a requirement. New modules should be enabled later with *drush* (`drush en module_name`) or using the web admin UI.

It is no recommended to include these module files in new commits, only updates to `composer.json` file and module configurations exported to *sync folder* should be included in your project repository.

### Including new Drupal modules in M-III
If the new module is going to be included in the M-III installation profile, it should be added to the dependency list inside the `composer.json` file of this repository and declared in the install module list in the profile `info.yml` file.
