M3 latest updates
=====================

V2.0.1-dev (No tag released; use dev-release branch header for latest updates):
- Added parameters for global identation layout in M3 theme
- Updated styles for content table in M3 theme header

V2.0.0 (No tag released; use dev-release branch header for latest updates):
- Added parameters SASS variables to configure themes easily
- Added styles for subtitule text types
- Updated subtitule stiles for content items to use new subtitule classes
- Updated classname for button effects (form .button to .btn-fx)
- Added text size override for larger buttons (.btn-lg)
- Changed text color for required indicator in form labels from "Info" to “Danger”
- Improved path alias translation configuration to fix related issues
- Updated translation configuration for paragraph types and infographic elements to avoid translation issues with paragraphs
- Added view global styles to M3 theme
- Added undestrike visual style to global visual styles in M3 theme
- Renamed class mtk-quote to m3-quote
- Improved box styles to avoid excesive overrides
- Improved form styles to avoid excesive overrides